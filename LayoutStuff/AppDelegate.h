//
//  AppDelegate.h
//  LayoutStuff
//
//  Created by Mike Finney on 10/7/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

